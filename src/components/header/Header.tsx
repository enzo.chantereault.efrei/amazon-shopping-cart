import React from 'react';
import './header.scss';

export interface HeaderProps {
    title: string;
}

export const Header: React.FC<HeaderProps> = ({title}) => {
    return (
        <div className='app-header'>
        <h1>{title}</h1>
      </div>
    );
}
