import React from 'react';
import { NumericFormat } from 'react-number-format';
import { shoppingItemEntity } from '../../models/shoping-item.entity';
import './shopping-cart.scss';

export interface ShoppingCartProps {
    cartItems: shoppingItemEntity[];
}

export const ShoppingCart: React.FC<ShoppingCartProps> = ({cartItems}) => {

    let cartTotal = 0;
    let cartLegth = 0;
    cartItems.forEach((item)=> {
        cartTotal += item.price * item.quantity;
        cartLegth += item.quantity;
    });

    return (
        <div className='shopping-cart-container'>
            <h3>Subtotal({cartLegth}&nbsp;items):
                <NumericFormat 
                    className='cart-total-price'
                    displayType={'text'} 
                    value={cartTotal} 
                    decimalSeparator={','}
                    decimalScale={2}
                    suffix={'€'}/>
            </h3>
            <button>Proceed to checkout</button>
        </div>
    );
}