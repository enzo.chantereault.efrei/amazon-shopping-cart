import React, { ChangeEvent, Dispatch, SetStateAction } from 'react';
import { shoppingItemEntity } from '../../models/shoping-item.entity';
import { ShoppingItem } from './shopping-item/shopping-item';
import './shopping-list.scss';

export interface ShoppingListProps {
    cartItems: shoppingItemEntity[];
    setCartItems: Dispatch<SetStateAction<shoppingItemEntity[]>>;
}

export const ShoppingList: React.FC<ShoppingListProps> = ({cartItems, setCartItems}) => {

    const changeItemQuantity = (newQuantity: number, index: number) => {
        let newCartItems: shoppingItemEntity[] = [...cartItems]; 
        if(!newQuantity) {
            newCartItems.splice(index, 1);
        } else {
            newCartItems[index].quantity = newQuantity;
        }
        setCartItems(newCartItems);
    }

    const itemList: JSX.Element[] = cartItems.map((cartItem, index) => <ShoppingItem key={index} cartItem={cartItem} index={index} changeItemQuantity={changeItemQuantity}/>);

    return (
        <div className='shopping-list-container'>
            <div className='card-items'>
                <h1>Shopping Cart</h1>
                <div className='Shopping-item-container'>
                    {itemList}
                </div>
            </div>
        </div>
    );
}