import React, { ChangeEvent } from 'react'
import { NumericFormat } from 'react-number-format';
import { shoppingItemEntity } from '../../../models/shoping-item.entity';
import './shopping-item.scss';

export interface ShoppingItemProps {
  cartItem: shoppingItemEntity;
  index:  number;
  changeItemQuantity: (newQuantity: number, index: number) => void;
}

export const ShoppingItem: React.FC<ShoppingItemProps> = ({cartItem, index, changeItemQuantity}) => {

  const cartItemImage: string = process.env.PUBLIC_URL + '/items/' + cartItem.image;
  const quatityValues: JSX.Element[] = [...Array(cartItem.stock).keys()].map((num, index) => <option key={index} value={num + 1}>Qty:&nbsp;{num + 1}</option>);

  return (
    <div className='shopping-item'>
      <div className='shopping-item-image'>
        <img src={cartItemImage} alt="" />
      </div>
      <div className='shopping-item-info'>
        <h2 className='info-title'>{cartItem.title}</h2>
        <div className='info-stock'>{cartItem.stock} In Stock</div>
        <div className='item-action'>
          <div className='item-quantity'>
            <select 
              value={cartItem.quantity}
              onChange={(event: ChangeEvent<HTMLSelectElement>) => {changeItemQuantity(+event.target.value as number, index)}}>
              {quatityValues}
            </select>
          </div>
          <div 
            className='item-delete' 
            onClick={() => {changeItemQuantity(0, index)}}>
              <span>Delete</span>
          </div>
        </div>
      </div>
      <NumericFormat 
        className='shopping-item-price' 
        displayType={'text'} 
        value={cartItem.price} 
        decimalSeparator={','}
        decimalScale={2}
        suffix={'€'}/>
    </div>
  )
}