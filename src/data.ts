import { shoppingItemEntity } from "./models/shoping-item.entity";

export const shoppingItems: shoppingItemEntity[] = [
    {
       title: "Apple iPad Pro",
       stock: 35,
       image: "item-1.jpg",
       price: 769,
       quantity: 1
    },
    {
        title: "Beats Flex Wireless Earphones",
        stock: 99,
        image: "item-2.jpg",
        price: 49.88,
        quantity: 2
     },
     {
        title: "De'Longhi La Specialista Espresso Machine with Knock Box",
        stock: 5,
        image: "item-3.jpg",
        price: 745.05,
        quantity: 1
     },
     {
        title: "Oculus Quest 2 — Advanced All-In-One Virtual Reality Headset",
        stock: 3,
        image: "item-4.jpg",
        price: 399,
        quantity: 1
     },
     {
        title: "All-new Echo Dot (4th Gen) | Glacier White",
        stock: 99,
        image: "item-5.jpg",
        price: 39.99,
        quantity: 3
     },
];