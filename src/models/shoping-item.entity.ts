export class shoppingItemEntity {
    title!: string;
    stock!: number;
    image!: string;
    price!: number;
    quantity!: number;
}
