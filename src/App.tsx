import React, {Dispatch, SetStateAction, useState} from 'react';
import './App.scss';
import { Header } from './components/header/header';
import { ShoppingCart } from './components/shopping-cart/shopping-cart';
import { ShoppingList } from './components/shopping-list/shopping-list';
import { shoppingItems } from './data'
import { shoppingItemEntity } from './models/shoping-item.entity';

const App = () => {

  const [cartItems, setCartItems]: [shoppingItemEntity[], Dispatch<SetStateAction<shoppingItemEntity[]>>] = useState(shoppingItems);

  return (
    <div className="app">
      <Header title="Amazon cart"/>
      <div className='app-body'>
        <ShoppingList 
          cartItems={cartItems}
          setCartItems={setCartItems}/>
        <ShoppingCart cartItems={cartItems}/>
      </div>
    </div>
  );
}

export default App;
